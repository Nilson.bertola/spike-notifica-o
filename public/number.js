let i = 0;

let interval = null;

clearInterval(interval);

const staticTitleNumber = value =>
  (document.title = `(${value}) ${initTitle}`);

const resetNumber = title => {
  clearInterval(interval);
  document.title = title;
  i = 0;
};

const blinkEffectTitle = value => {
  const blinkNotifyMsg = `${value} Notificações`;
  interval = setInterval(() => {
    i++;
    if (i % 2 === 0) return (document.title = initTitle);
    document.title = blinkNotifyMsg;
  }, 1500);
};
