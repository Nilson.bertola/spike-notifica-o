let secure = true;
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("./sw.js");
} else {
  secure = false;
}
// console.log(error);
// alert("ERR :: 2 :: Funcionalidade não suportada");

console.log(navigator.userAgent);

const icon = `./heart.png`;
const badge = `./bell.png`;
const requestPerm = async () => {
  await Notification.requestPermission();
  console.log(Notification.permission);
  return Notification.permission;
};

const makeNotification = (text, options, timer = 0) =>
  setTimeout(() => {
    console.log(secure);
    if (secure) {
      navigator.serviceWorker.ready.then(reg => {
        if ("showNotification" in reg) {
          console.log("service worker");
          reg.showNotification(text, options);
        } else {
          new Notification(text, options);
        }
      });
    } else {
      new Notification(text, options);
    }
    // try {
    // } catch (error) {
    // alert("ERR :: 16 :: Funcionalidade não suportada");
    // }
    // try {
    // } catch (error) {
    //   ServiceWorkerRegistration.showNotification(text, options);
    // }
  }, timer);

const simpleNotify = async (value, timeout = 0) => {
  if ((await requestPerm()) === "granted") {
    makeNotification(`${value} Novas Mensagens!`, {}, timeout);
  }
};

const icoNotify = async value => {
  if ((await requestPerm()) === "granted") {
    makeNotification(`${value} Novas Mensagens!`, {
      icon
    });
  }
};

const IBNotify = async value => {
  if ((await requestPerm()) === "granted") {
    makeNotification(`${value} Novas Mensagens!`, {
      icon,
      badge
    });
  }
};
